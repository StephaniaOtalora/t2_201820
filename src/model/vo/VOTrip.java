package model.vo;


/**
 * Representation of a Trip object
 */
public class VOTrip {

	private int tripId;

	private String dStart;

	private String dEnd;

	private int bikeId;

	private double duration;

	private String stFrom;

	private int stFromId;

	private String stTo;

	private int stToId;

	private String userType;

	private String gender;

	private int birthday;

	public VOTrip(int pTrpId, String pdStart, String pdEnd,int pbikeId, double pDur, String pstFr, int pstFrId,  String pstTo, int pstToId, String pUser, String pGender, int pBirth )
	{
		tripId = pTrpId;
		dStart = pdStart;
		dEnd = pdEnd;
		bikeId = pbikeId;
		duration = pDur;
		stFrom = pstFr;
		stFromId = pstFrId;
		stTo = pstTo;
		stToId = pstToId;
		userType = pUser;
		gender = pGender;
		birthday = pBirth;
	}
	/**
	 * @return id - Trip_id
	 */
	public int id() {
		// TODO Auto-generated method stub
		return tripId;
	}	


	/**
	 * @return time - Time of the trip in seconds.
	 */
	public double getTripSeconds() {
		// TODO Auto-generated method stub
		return duration;
	}

	/**
	 * @return station_name - Origin Station Name .
	 */
	public String getFromStation() {
		// TODO Auto-generated method stub
		return stFrom;
	}

	/**
	 * @return station_name - Destination Station Name .
	 */
	public String getToStation() {
		// TODO Auto-generated method stub
		return stTo;
	}
	
	public int getToStationId() {
		// TODO Auto-generated method stub
		return stToId;
	}
	

	public String getGender(){
		return gender;
	}
}
