package model.data_structures;

import java.util.Iterator;

public class DoublyLinkedList<T extends Object> implements IDoublyLinkedList<T> 
{

	int size;

	Node current;

	Node head;

	Node end;

	public DoublyLinkedList() 
	{
		size = 0;
		current = null;
		head = null;
		end = null;
	}
	@Override
	public int getSize() {
		// TODO Auto-generated method stub
		return size;
	}

	@Override
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean add(T pT) {
		// TODO Auto-generated method stub
		boolean check = false;
		Node<T> ne = new Node(null, null, pT);
		if(head==null)
		{
			head = ne;
			end = head;
			check = true;
			current = head;
			if(check) size++;
		}
		else
		{
			check = addAtEnd(pT);
		}


		return check;
	}

	@Override
	public boolean addAtEnd(T pT) {
		// TODO Auto-generated method stub

		boolean check = false;
		Node<T> ne = new Node(end, null, pT);

		end.changeNext(ne);
		end = ne;
		check = true;

		if(check) size++;

		return check;
	}

	@Override
	public boolean addAtK(int k, T pT) {

		boolean check = false;
		Node<T> ne = new Node(null, null, pT);

		Node<T> cur = head;

		if(k == 1)
		{
			ne.changeNext(cur);
			head.changePrev(ne);
			head = ne;
			check = true;
		}
		else if(k<=size)
		{

			while(k > 1 && cur.getNext() !=null)
			{
				cur = cur.getNext();
				k--;
			}

			cur.getPrev().changeNext(ne);
			ne.changePrev(cur.getPrev());
			ne.changeNext(cur);
			cur.changePrev(ne);

			check = true;
		}

		if(check) size++;

		return check;
	}

	@Override
	public T getCurrentElement() 
	{
		// TODO Auto-generated method stub
		return (T) current.getElement();
	}

	@Override
	public T getElement(int k) 
	{
		Node<T> cur = head;
		T elem = null;

		if(k<=size)
		{
			while(k > 1)
			{
				cur = cur.getNext();
				k--;
			}
			elem = cur.getElement();
		}
		return elem;
	}

	@Override
	public boolean delete(T pT) {
		// TODO Auto-generated method stub
		boolean check = false;
		Node<T> cur = head;

		if(head.getElement().equals(pT) && !head.getElement().equals(end))
		{
			head.getNext().changePrev(null);
			head = head.getNext();
			check = true;			
		}
		else if(end.getElement().equals(pT))
		{
			end = end.getPrev();
			end.changeNext(null);
			check = true;
		}
		else
		{
			while(!check && cur!=null )
			{
				if(pT.equals(cur.getElement()))
				{
					cur.getPrev().changeNext(cur.getNext());
					cur.getNext().changePrev(cur.getPrev());
					check = true;

				}
				cur = cur.getNext();
			}
		}

		if(check) size--;

		return check;

	}

	@Override
	public boolean deleteAtk(int k) {
		// TODO Auto-generated method stub
		boolean check = false;

		Node<T> cur = head;

		if(k<=size)
		{
			while(k > 1)
			{
				cur = cur.getNext();
				k--;
			}

			cur.getPrev().changeNext(cur.getNext());
			cur.getNext().changePrev(cur.getPrev());

			if(cur.getNext()==null)
			{
				end = cur.getNext();
			}

			check = true;
		}

		if(check) size--;

		return check;
	}
	@Override
	public Node getNext() {
		// TODO Auto-generated method stub
		return current.getNext();
	}
	@Override
	public Node getPrevious() {
		// TODO Auto-generated method stub
		return current.getPrev();
	}



}
