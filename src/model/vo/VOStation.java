package model.vo;

/**
 * Representation of a byke object
 */
public class VOStation 
{
	private int id;
	
	private String name;
	
	private String city;
	
	private double latitude;
	
	private double longitude;
	
	private int capacity;
	
	private String onDate;
	

	public VOStation(int pId, String pName, String pCity, double pLatitude, double pLongitud, int pCapacity, String pDate)
	{
		id = pId;
		name = pName;
		city = pCity;
		latitude = pLatitude;
		longitude = pLongitud;
		capacity = pCapacity;
		onDate = pDate;
	}
	
	/**
	 * @return id_bike - Bike_id
	 */
	public int id() {
		// TODO Auto-generated method stub
		return id;
	}	
}
