package model.logic;

import java.io.FileReader;
import com.opencsv.CSVReader;

import api.IDivvyTripsManager;
import model.vo.VOStation;
import model.vo.VOTrip;
import model.data_structures.DoublyLinkedList;
import model.data_structures.IDoublyLinkedList;

public class DivvyTripsManager implements IDivvyTripsManager {

	private DoublyLinkedList<VOTrip> trips;

	private DoublyLinkedList<VOStation> stations;



	public void loadStations (String stationsFile)
	{
		// TODO Auto-generated method stub
		try
		{
			CSVReader reader = new CSVReader(new FileReader(stationsFile));
			String [] nextLine = reader.readNext();
			while (nextLine != null) 
			{
				if(!nextLine[1].equals("id"))
				{
					int pId = Integer.parseInt(nextLine[1]);
					double pLatitude = Double.parseDouble(nextLine[4]);
					double pLongitud = Double.parseDouble(nextLine[5]);
					int pCapacity = Integer.parseInt(nextLine[6]);
					VOStation ne = new VOStation(pId, nextLine[2], nextLine[3], pLatitude, pLongitud, pCapacity, nextLine[7]);
					stations.add(ne);
				}
				nextLine = reader.readNext();
			}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}

	}


	public void loadTrips (String tripsFile) 
	{
		// TODO Auto-generated method stub
		try
		{
			CSVReader reader = new CSVReader(new FileReader(tripsFile));
			String [] nextLine = reader.readNext();
			while (nextLine != null) 
			{
				if(!nextLine[1].equalsIgnoreCase("trip_id"))
				{
					int pId = Integer.parseInt(nextLine[1]);
					int pBikeId = Integer.parseInt(nextLine[4]);
					double pdur = Double.parseDouble(nextLine[5]);
					int pstFrId = Integer.parseInt(nextLine[7]);
					int pstToId = Integer.parseInt(nextLine[9]);
					int pBirth = Integer.parseInt(nextLine[12]);
					VOTrip ne = new VOTrip(pId, nextLine[2], nextLine[3], pBikeId, pdur, nextLine[6], pstFrId, nextLine[8], pstToId, nextLine[10], nextLine[11], pBirth);
					trips.add(ne);

				}
				nextLine = reader.readNext();
			}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}

	@Override
	public IDoublyLinkedList <VOTrip> getTripsOfGender (String gender) {
		// TODO Auto-generated method stub

		DoublyLinkedList<VOTrip> tripsGender = null;
		VOTrip cu = trips.getCurrentElement();
		while(cu!=null)
		{
			if(trips.getCurrentElement().getGender().equalsIgnoreCase(gender));
			{
				tripsGender.add(cu);
			}
			cu = (VOTrip) trips.getNext().getElement();
		}
		return tripsGender;
	}

	@Override
	public IDoublyLinkedList <VOTrip> getTripsToStation (int stationID) {
		// TODO Auto-generated method stub
		DoublyLinkedList<VOTrip> tripsToSt = null;
		VOTrip cu = trips.getCurrentElement();
		while(cu!=null)
		{
			if(trips.getCurrentElement().getToStationId() == stationID);
			{
				tripsToSt.add(cu);
			}
			cu = (VOTrip) trips.getNext().getElement();
		}
		return tripsToSt;
	}


	@Override
	public void loadServices(String stationsFile) 
	{
		// TODO Auto-generated method stub




	}


}
