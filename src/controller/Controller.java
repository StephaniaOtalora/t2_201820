package controller;

import api.IDivvyTripsManager;
import model.data_structures.IDoublyLinkedList;
import model.logic.DivvyTripsManager;
import model.vo.VOStation;
import model.vo.VOTrip;

public class Controller {

	/**
	 * Reference to the services manager
	 */
	private static IDivvyTripsManager  manager = new DivvyTripsManager();
	
	public static void loadStations() {
		
	}
	
	public static void loadTrips() {
		
	}
		
	public static IDoublyLinkedList <VOTrip> getTripsOfGender (String gender) {
		return null;
	}
	
	public static IDoublyLinkedList <VOTrip> getTripsToStation (int stationID) {
		return null;
	}
}
