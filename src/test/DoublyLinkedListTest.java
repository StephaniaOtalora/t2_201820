package test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import model.data_structures.DoublyLinkedList;

public class DoublyLinkedListTest 
{

	private DoublyLinkedList<Object> lista;

	public void setUpEscenario1()
	{
		lista = new DoublyLinkedList<Object>();
	}

	@Test
	public void testDatos( )
	{
		setUpEscenario1();
		assertEquals( "El tama�o es incorrecto", 0, lista.getSize() );
	}

	@Test
	public void testAdd( )
	{
		setUpEscenario1();
		Object ele = new Object();
		Object ele1 = new Object();
		Object ele2 = new Object();

		lista.add(ele);
		lista.add(ele1);
		lista.add(ele2);

		assertEquals( "El tama�o es incorrecto", 3, lista.getSize() );
		assertEquals( "El elemento actual es incorrecto", ele, lista.getCurrentElement() );

		lista.add(ele2);
		assertEquals( "El tama�o es incorrecto", 4, lista.getSize() );
	}

	@Test
	public void testAddAtK( )
	{
		setUpEscenario1();
		Object ele = new Object();
		Object ele1 = new Object();
		Object ele2 = new Object();
		Object ele3 = new Object();
		Object ele4 = new Object();

		lista.add(ele);
		lista.add(ele1);
		lista.add(ele2);

		assertEquals( "El tama�o es incorrecto", 3, lista.getSize() );

		lista.addAtK(2, ele3);
		assertEquals( "El elemento es incorrecto", ele3, lista.getElement(2));

		lista.addAtK(3, ele4);
		assertEquals( "El elemento es incorrecto", ele4, lista.getElement(3));


	}

	@Test
	public void testAddAtEnd( )
	{
		setUpEscenario1();
		Object ele = new Object();
		Object ele1 = new Object();
		Object ele2 = new Object();
		Object ele3 = new Object();
		Object ele4 = new Object();

		lista.add(ele4);
		lista.add(ele3);
		lista.add(ele2);

		assertEquals("El elemento es incorrecto", ele2, lista.getElement(3));

		lista.addAtEnd(ele1);
		assertEquals("El elemento es incorrecto",ele1 , lista.getElement(4));

		lista.addAtEnd(ele);
		assertEquals("El elemento es incorrecto",ele , lista.getElement(5));
	}

	@Test
	public void testGetElement( )
	{
		setUpEscenario1();
		Object ele = new Object();
		Object ele1 = new Object();
		Object ele2 = new Object();

		lista.add(ele1);
		assertEquals("El elemento es incorrecto",ele1 , lista.getElement(1));

		lista.add(ele);
		assertEquals("El elemento es incorrecto",ele , lista.getElement(2));

		lista.add(ele2);
		assertEquals("El elemento es incorrecto",ele2 , lista.getElement(3));
	}

	@Test
	public void testDelete( )
	{
		setUpEscenario1();
		Object ele = new Object();
		Object ele1 = new Object();
		Object ele2 = new Object();

		lista.add(ele);
		lista.add(ele1);
		lista.add(ele2);

		assertEquals( "El tama�o es incorrecto", 3, lista.getSize() );
		assertEquals( "El elemento actual es incorrecto", ele, lista.getCurrentElement() );

		lista.delete(ele2);
		assertEquals( "El tama�o es incorrecto", 2, lista.getSize() );

		lista.delete(ele1);
		assertEquals( "El tama�o es incorrecto", 1, lista.getSize() );


	}

	@Test
	public void testDeleteAtK( )
	{
		setUpEscenario1();
		Object ele = new Object();
		Object ele1 = new Object();
		Object ele2 = new Object();
		Object ele3 = new Object();
		Object ele4 = new Object();

		lista.add(ele);
		lista.add(ele1);
		lista.add(ele2);
		lista.add(ele3);
		lista.add(ele4);

		assertEquals( "El tama�o es incorrecto", 5, lista.getSize() );

		lista.deleteAtk(3);
		assertEquals( "El tama�o es incorrecto", 4, lista.getSize() );

		lista.deleteAtk(2);
		assertEquals( "El tama�o es incorrecto", 3, lista.getSize() );


	}


}
