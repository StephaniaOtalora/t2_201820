package model.data_structures;

public class Node <T extends Object>
{

	private Node prev;
	private Node next;
	private T element;

	public Node(Node pPrev, Node pNext, T pElement )
	{
		prev= pPrev;
		next=pNext;
		element = pElement;

	}

	public Node getPrev()
	{
		return prev;

	}

	public Node getNext()
	{
		return next;
	}

	public void changeNext(Node pNext)
	{
		next = pNext;
	}

	public void changePrev(Node pPrev)
	{
		prev = pPrev;
	}

	public T getElement()
	{
		return element;
	}

}
